using System;
using System.Diagnostics;
using System.IO;
using Unity.Build.Desktop.DotsRuntime;
using Unity.Build.DotsRuntime;
using Unity.Build.Internals;

namespace Unity.Build.Windows.DotsRuntime
{
    abstract class WindowsBuildTarget : BuildTarget
    {
        public override Platform Platform => Platform.Windows;
        public override bool CanBuild => UnityEngine.Application.platform == UnityEngine.RuntimePlatform.WindowsEditor;
        public override string ExecutableExtension => ".exe";
        public override bool IsDesktopBuildTarget => true;

        public override BuildTarget CreateDesktopBuildTargetFromType(DesktopTargetType type = DesktopTargetType.DotNet)
        {
            switch (type)
            {
                case DesktopTargetType.DotNet:
                    return new DotNetTinyWindowsBuildTarget();
                case DesktopTargetType.DotNetStandard_2_0:
                    return new DotNetStandard20WindowsBuildTarget();
                case DesktopTargetType.Il2cpp:
                    return new IL2CPPWindowsBuildTarget();
                default:
                    return new UnknownBuildTarget();
            }
        }

        public override bool Run(FileInfo buildTarget)
        {
            var startInfo = new ProcessStartInfo();
            startInfo.FileName = buildTarget.FullName;
            startInfo.WorkingDirectory = buildTarget.Directory.FullName;

            return new DesktopRun().RunOnThread(startInfo);
        }

        internal override ShellProcessOutput RunTestMode(string exeName, string workingDirPath, int timeout)
        {
            var shellArgs = new ShellProcessArguments
            {
                Executable = $"{workingDirPath}/{exeName}.exe",
                Arguments = new string[] { },
            };

            return DesktopRun.RunTestMode(shellArgs, workingDirPath, timeout);
        }
    }

    sealed class DotNetTinyWindowsBuildTarget : WindowsBuildTarget
    {
#if UNITY_EDITOR_WIN
        protected override bool IsDefaultBuildTarget => true;
#endif

        public override string DisplayName => "Windows .NET - Tiny";
        public override string BeeTargetName => "windows-dotnet";
        public override bool UsesIL2CPP => false;
        public override string DefaultAssetFileName => "Win";
        public override bool ShouldCreateBuildTargetByDefault => true;
    }

    sealed class DotNetStandard20WindowsBuildTarget : WindowsBuildTarget
    {
        public override string DisplayName => "Windows .NET - .NET Standard 2.0";

        public override string BeeTargetName => "windows-dotnet-ns20";

        public override bool UsesIL2CPP => false;
    }

    sealed class IL2CPPWindowsBuildTarget : WindowsBuildTarget
    {
        public override string DisplayName => "Windows IL2CPP - Tiny";
        public override string BeeTargetName => "windows-il2cpp";
        public override bool UsesIL2CPP => true;
    }
}
